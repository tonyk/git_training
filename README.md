# Git training: changing the history

The goal of this exercise is to train how to play with `git rebase`. You will
need to use almost all options from rebase in order do complete this challenge.
Use `git log --oneline` to check the commit messages. They have tips of what
need to be done at each commit.

To start, use `git rebase -i --root` to be able to modify all commits since the
beginning of the repository. The editor will provide the available options at
the file's bottom. The challenge will be completed when you have at your log:

- All `order: ...` commits are crescent ordered
- The `reword: ...` commit message has no typos
- The `join: ...` commit message is completed
- The text commited at `fix: ...` has no typos
- The commit `split: ...` is split in two commits

To check the answer, have a look at the branch `solved`. Good luck!
